<?php

namespace Extractiv\Entities;
use Extractiv\System\Entity;
use Extractiv\Entities\Address;

/**
 * Example
 *
 */
class Example extends Entity {

    const DATABASE_TABLE_NAME = 'table-name';
    const DATABASE_ID_LABEL = 'id';

    private $data_id;
    private $data_firstname = Entity::REQUIRED;
    private $data_lastname = Entity::REQUIRED;
    private $data_email;
    private $data_ref_address; 

    /* Constructor */
    function __construct($_id=null, $_hydrate=Entity::INIT_HYDRATE_ON)
    {
        parent::__construct(
            $_id,
            $_hydrate,
            self::DATABASE_TABLE_NAME,
            self::DATABASE_ID_LABEL,
            $this);
    }

    /* getFullName */
    public function getFullName ()
    {
        if (!$this->isIntoDatabase())
            return Entity::NC_PROPERTY_TERM;

        return ($this->getElement('firstname') . ' ' . $this->getElement('lastname'));
    }

    /* getAddress */
    public function getAddress()
    {
        if($this->data_ref_adress == null){
            throw new \Exception(get_class($this) . ' : Missing Data (object not hydrated ?)');
            return false;
        }

        return new Address($this->data_ref_address);
    }
}
