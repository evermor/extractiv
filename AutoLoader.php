<?php

/**
 * Load bundles by namespace
 *
 * @author Evermor
 */
abstract class AutoLoader {

    public static function register(){
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    public static function autoload($_class){

        $root = dirname(__FILE__) . DIRECTORY_SEPARATOR;
        $parts = preg_split('#\\\#', $_class);
        $className = array_pop($parts);
        $path = implode(DIRECTORY_SEPARATOR, $parts);
        $file = $className . '.php';
        $filepath = $root . $path . DIRECTORY_SEPARATOR . $file;

        if(!file_exists($filepath)){
            return false;
        }

        require $filepath;
        return true;
    }
}
