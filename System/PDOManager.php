<?php

namespace Extractiv\System;

use \PDO;
use \Exception;

//************************************************************************************************************
// CLASS PDOManager
//------------------------------------------------------------------------------------------------------------
// evermor
//------------------------------------------------------------------------------------------------------------
// 11/02/2013
//------------------------------------------------------------------------------------------------------------
// v 1.0
//************************************************************************************************************
class PDOManager extends PDO
{
    const ACCURATE_CONDITION_OFF = 0;
    const ACCURATE_CONDITION_ON = 1;

    protected static $instance; // variable contenant une instance unique ( singleton )
    private $dsn;
    private $username;
    private $password;

    //--------------------------------------------------------------------------------------------------------
    // CONSTRUCTEUR
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    function __construct()
    {
        // Initialisations
        $this->dsn = DSN;
        $this->username = USERNAME;
        $this->password = PASSWORD;

        $this->connect();
    } // Fin constructeur

    //--------------------------------------------------------------------------------------------------------
    // SINGLETON getInstance
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    public static function getInstance()
    {
        if(!isset( self::$instance )){
            self::$instance = new PDOManager();
        }

        return self::$instance;
    }

    //--------------------------------------------------------------------------------------------------------
    // getLastIdFrom
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    public function getLastIdFrom($_table)
    {
        $sql  = "SELECT LAST_INSERT_ID() INTO @" . $_table . ";";
        $pdos = $this->prepare($sql);

        if(!$pdos->execute()){
            throw new Exception(get_class( $this ).' : Erreur lors de la requête SQL : ' . $sql );
            ErrorLogger::Log(ErrorLogger::E_USER_ERROR, $e->getMessage(), __FILE__, $sql);
            return false;
        }

        $value = $pdos->fetchColumn();
        $pdos->closeCursor();

        return $value;
    }

    //--------------------------------------------------------------------------------------------------------
    // connect
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    private function connect()
    {
        parent::__construct( $this->dsn, $this->username, $this->password );	// Appel du constructeur parent
        $this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );		// Définition du mode d'erreur
        $this->exec( "SET CHARACTER SET utf8" );								// Prise en charge d'UTF-8 pour la connexion

        return true;
    }

    //--------------------------------------------------------------------------------------------------------
    // insert
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    public function insert( $sTable='', $aElms=array() )
    {
        // traitements
        foreach($aElms as $key => $elm){
            $aElms2[$key] = addslashes($elm);
        }

        $sql  = "INSERT INTO `".$sTable."`";
        $sql .= " (`".implode( "`, `", array_keys( $aElms2 ) )."`)";
        $sql .= " VALUES ('".implode( "', '", $aElms2 )."') ";

        $datas = $this->executeQuery($sql, false);
        return $datas;
    }

    //--------------------------------------------------------------------------------------------------------
    // update
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    public function update( $sTable='', $aElms=array(), $aConditions )
    {
        if( isset($aConditions) && !empty($aConditions) )
        {
            // traitements
            foreach($aElms as $key => $elm){
                $aElms2[$key] = addslashes($elm);
            }

            $sConditions = '';
            foreach( $aConditions as $field => $value ){
                $sConditions .= ' AND `'.$field.'` = "'.$value.'"';
            }

            $sql  = "UPDATE `".$sTable."`";
            $sql .= "SET ";
            foreach($aElms2 as $key => $value){
                $s[] = "`".$key."` = '".$value."'";
            }
            $sql .= implode( ',', $s );
            $sql .= " WHERE 1=1 ".$sConditions;

            $datas = $this->executeQuery($sql, false);
            return $datas;

        }	else{
            throw new Exception('Pas de conditions définies');
        }
    }

    //--------------------------------------------------------------------------------------------------------
    // select
    //--------------------------------------------------------------------------------------------------------
    // Select simple
    // conditions soud la forme clé => valeur
    //--------------------------------------------------------------------------------------------------------
    public function select( $sTable='', $aFields=null, $aConditions=null, $aGroupBy=null, $aOrderBy=null, $bOrderType=false, $iLimit=null)
    {
        $sConditions = '';
        $sGroupBy = '';
        $sOrderBy = '';
        $sLimit = '';

        if( $aFields != null ){
            $sFields = implode( ', t.', $aFields );
        }	else{
            $sFields = '*';
        }
        if( $aConditions != null ){
            foreach( $aConditions as $field => $value ){
                $sConditions .= ' AND `'.$field.'` = "'.$value.'" ';
            }
        }
        if( $aGroupBy != null ){
            $sGroupBy .= 'GROUP BY ';
            $sGroupBy .= implode(',', $aGroupBy).' ';
        }
        if( $aOrderBy != null ){
            $sOrderBy .= 'ORDER BY ';
            $sOrderBy .= implode(',', $aOrderBy) . ( $bOrderType ? ' ASC ' : ' DESC ' );
        }
        if( $iLimit != null ){
            $sLimit .= 'LIMIT '.$iLimit;
        }

        // Assemblage de la requête
        $sql = 'SELECT '.$sFields.' FROM `'.$sTable.'` t WHERE 1 '
            .$sConditions
            .$sGroupBy
            .$sOrderBy
            .$sLimit;

        // Execution
        $datas = $this->executeQuery($sql);

        // Retour des données
        return $datas;
    }

    //--------------------------------------------------------------------------------------------------------
    // selectAccurate
    //--------------------------------------------------------------------------------------------------------
    // Select avec plus de précision dans les conditions
    // conditions sous forme de tableau de valeurs (pas de clé)
    //--------------------------------------------------------------------------------------------------------
    public function selectAccurate($_sTable='', $_aFields=null, $_aConditions=null, $_aGroupBy=null, $_aOrderBy=null, $_bOrderType=false, $_iLimit=null, $_aJoins=null, $_offset=null)
    {
        if(!empty($_aGroupBy) && !is_array($_aGroupBy)){
            throw new Exception(get_class( $this ).' : Type de paramètre non attendu : ' . $_aGroupBy . ' doit être sous forme de tableau de données' );
        }

        if(!empty($_aOrderBy) && !is_array($_aOrderBy)){
            throw new Exception(get_class( $this ).' : Type de paramètre non attendu : ' . $_aOrderBy . ' doit être sous forme de tableau de données' );
        }

        $sFields        = ($_aFields != null) ? implode(',', $_aFields) : '*';
        $sConditions    = ($_aConditions != null) ? ' AND ' . implode(' AND ', $_aConditions) : '';
        $sGroupBy       = ($_aGroupBy != null) ? ' GROUP BY ' . implode(',', $_aGroupBy) . ' ' : '';
        $sOrderBy       = ($_aOrderBy != null) ? ' ORDER BY ' . implode(',', $_aOrderBy) . ($_bOrderType ? ' ASC ' : ' DESC ') : '';
        $sLimit         = ($_iLimit != null) ? ' LIMIT ' . (($_offset != null) ? $_offset . ', ' : '') . $_iLimit : '';

        $sJoins = '';
        if($_aJoins != null){
            foreach ($_aJoins as $table => $relation){
                $sJoins .= 'LEFT JOIN `' . $table . '` ON ' . $relation . ' ';
            }
        }

        $sql = 'SELECT ' .
            $sFields . ' FROM `' .
            $_sTable . $sJoins . '` WHERE 1 ' .
            $sConditions .
            $sGroupBy .
            $sOrderBy .
            $sLimit;

        try{
            $datas = $this->executeQuery($sql);
        }   catch (Exception $e){
            echo $e->getCode() . $e->getMessage() . $sql;
            return false;
        }

        return $datas;
    }

    //--------------------------------------------------------------------------------------------------------
    // executeQuery
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    public function executeQuery($sql, $bSelect=true)
    {
        $pdos = $this->prepare($sql);
        if(!$pdos->execute()){
            throw new Exception(get_class( $this ).' : Erreur lors de la requête SQL : ' . $sql );
            return false;
        }

        if($bSelect === true){
            $datas = $pdos->fetchAll();
            $pdos->closeCursor();
            return($datas);
        }	else{
            $pdos->closeCursor();
            return true;
        }
    }

    //--------------------------------------------------------------------------------------------------------
    // checkForExistingInsert
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    public function checkForExistingInsert( $table, $aInsert )
    {
        $sql = 'SELECT * FROM `'.$table.'` WHERE 1';
        foreach( $aInsert as $field => $value ){ $sql .= ' AND `'.$field.'` = "'.$value.'"'; }
        $sql .= ';';

        $pdos = $this->prepare( $sql );
        if( !$pdos->execute() ){
            throw new Exception( get_class( $this ).' : Erreur lors de la vérification de données présentes en base' );
        }

        if( $pdos->rowCount() > 0 ){
            $datas = $pdos->fetchAll();
            $pdos->closeCursor();
            return $datas;
        }	else{
            $pdos->closeCursor();
            return false;
        }
    }

}

?>
