<?php

/**
 * Entity
 *
 * @author Evermor
 */

namespace Extractiv\System;
use Extractiv\Entities\Project;
use Extractiv\Entities\DocumentProjet;
use Extractiv\Entities\LinkProjetClient;
use Extractiv\Entities\LinkOffreArticle;
use Extractiv\Entities\LinkOffreProfessionnel;
use Extractiv\Entities\Utilisateur;
use Extractiv\Entities\Client;
use Extractiv\Entities\Offre;
use Extractiv\Entities\Comptabilite;
use Extractiv\Entities\Professionnel;
use Extractiv\Entities\DocumentProfessionnel;
use Extractiv\Entities\ProcesVerbal;
use Extractiv\Entities\Devis;
use Extractiv\Entities\Metier;
use Extractiv\Entities\Article;
use Extractiv\Entities\Assurance;

abstract class Entity {

    const REQUIRED = 'meta|required';
    const INIT_HYDRATE_OFF = 0;
    const INIT_HYDRATE_ON = 1;
    const NC_PROPERTY_TERM = 'Non renseigné';
    const INHERITING_CLASS_PROJECT = "\Extractiv\Entities\Project";
    const INHERITING_CLASS_DOCUMENT_PROJET = "\Extractiv\Entities\DocumentProjet";
    const INHERITING_CLASS_LINK_PROJET_CLIENT = "\Extractiv\Entities\LinkProjetClient";
    const INHERITING_CLASS_LINK_OFFRE_ARTICLE = "\Extractiv\Entities\LinkOffreArticle";
    const INHERITING_CLASS_LINK_OFFRE_PROFESSIONNEL = "\Extractiv\Entities\LinkOffreProfessionnel";
    const INHERITING_CLASS_UTILISATEUR = "\Extractiv\Entities\Utilisateur";
    const INHERITING_CLASS_CLIENT = "\Extractiv\Entities\Client";
    const INHERITING_CLASS_OFFRE = "\Extractiv\Entities\Offre";
    const INHERITING_CLASS_COMPTABILITE = "\Extractiv\Entities\Comptabilite";
    const INHERITING_CLASS_PROFESSIONNEL = "\Extractiv\Entities\Professionnel";
    const INHERITING_CLASS_DOCUMENT_PROFESSIONNEL = "\Extractiv\Entities\DocumentProfessionnel";
    const INHERITING_CLASS_PROCESVERBAL = "\Extractiv\Entities\ProcesVerbal";
    const INHERITING_CLASS_DEVIS = "\Extractiv\Entities\Devis";
    const INHERITING_CLASS_METIER = "\Extractiv\Entities\Metier";
    const INHERITING_CLASS_ARTICLE = "\Extractiv\Entities\Article";
    const INHERITING_CLASS_ASSURANCE = "\Extractiv\Entities\Assurance";

    protected $id;
    protected $pdom;
    protected $reflect;
    protected $db_table_label;
    protected $db_id_label;
    protected $childRef;
    protected $flag_datas_checked = false;
    protected $flag_database_matched = false;
    protected $aRequiredProperties;

    private static $aLastId = array();
    private static $iLastOverCount;
    private $aUpdatedProperties = array();

    //--------------------------------------------------------------------------------------------------------
    // Constructeur
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    function __construct($_id=null, $_hydrate=Entity::INIT_HYDRATE_ON, $_db_table_label=null, $_db_id_label=null, $_childRef)
    {
        if ($_db_table_label === null || $_db_id_label === null){
            throw new \Exception(get_class($this) . ' : valeurs manquantes à l\'initialisation');
        }

        $this->id = $_id;
        $this->db_table_label = $_db_table_label;
        $this->db_id_label = $_db_id_label;
        $this->childRef = $_childRef;
        $this->reflect = new \ReflectionClass($_childRef);
        $this->pdom = PDOManager::getInstance();

        // Si l'id est renseigné + INIT_HYDRATE_ON on récupère les données en bdd
        if(!empty($this->id) && $_hydrate==Entity::INIT_HYDRATE_ON){
            $this->getDatas();
        }
    }

    /*
     * cloneEntity
     * $_db_init = insert en base de donnée si true
     */
    public function cloneEntity($_db_init=false){
        $type = get_class($this->childRef);
        $oClone = new $type();

        $properties = $this->reflect->getProperties(\ReflectionProperty::IS_PRIVATE);
        foreach($properties as $property)
        {
            $field_name = str_replace('data_', '', $property->name);
            if ($field_name != $this->db_id_label){
                if (substr($property->name, 0, 5) === 'data_'){
                    $prop = $this->reflect->getProperty($property->name);
                    $prop->setAccessible(true);
                    $value = $prop->getValue($this->childRef);
                    $oClone->setElement($field_name, $value);
                }
            }
        }

        if($_db_init){ $oClone->initialize(); }
        return $oClone;
    }

    /*
     * forceIdToNull
     */
    private function forceIdToNull(){
        $this->id = null;
        $realName = 'data_' . $this->db_id_label;
        $prop = $this->reflect->getProperty($realName);
        $prop->setAccessible(true);
        if(property_exists($this->childRef, $realName)){
            $prop->setValue($this->childRef, null);
        }	else{
            throw new \Exception('class ' . get_class($this) .
                ' : Eléments non existant ou non accessible : ' . $_property);
            return false;
        }
    }

    //--------------------------------------------------------------------------------------------------------
    // getlastId
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    public function getlastId ()
    {
        $inheritingClassName = get_class($this->childRef);
        if (!isset(self::$aLastId[$inheritingClassName])){
            $value = $this->pdom->getLastIdFrom($this->db_table_label);
            if (!$value){
                return false;
            }

            self::$aLastId[$inheritingClassName] = $value;
        }

        return self::$aLastId[$inheritingClassName];
    }

    //--------------------------------------------------------------------------------------------------------
    // getLastOverCount
    //--------------------------------------------------------------------------------------------------------
    // Retourne la dernière valeur correspondant aux nombre d'enregistrements total en base de données
    // dont la requête correspondante à fait l'objet d'une clause LIMIT.
    //--------------------------------------------------------------------------------------------------------
    public static function getLastOverCount (){
        return self::$iLastOverCount;
    }

    //--------------------------------------------------------------------------------------------------------
    // getNextId
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    public function getNextId (){
        return self::getlastId() + 1;
    }

    //--------------------------------------------------------------------------------------------------------
    // setLastId
    //--------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------
    private function setLastId ($_id){
        $inheritingClassName = get_class($this->childRef);
        self::$aLastId[$inheritingClassName] = $_id;
    }

    /*
     * flushDatas
     */
    public function flushDatas()
    {
        if($this->id == null){
            $this->initialize();
        } else{
            $this->update();
        }
    }

    //--------------------------------------------------------------------------------------------------------
    // initialize
    //--------------------------------------------------------------------------------------------------------
    // Insert en Base de données l'entité avec les valeurs initialisées via la méthode setElement.
    // Les propriétés requises pour l'initialisation doivent être initialisées à Entity::REQUIRED dans la
    // classe enfant. Si elles ne sont pas renseignées à l'initialisation, une exception est lancée et l'enre-
    // gistrement en BDD échoue.
    //--------------------------------------------------------------------------------------------------------
    public function initialize()
    {
        $aDatas = array();

        $properties = $this->reflect->getProperties(\ReflectionProperty::IS_PRIVATE);
        foreach($properties as $property)
        {
            if (substr($property->name, 0, 5) === 'data_'){
                $prop = $this->reflect->getProperty($property->name);
                $prop->setAccessible(true);
                $value = $prop->getValue($this->childRef);
                if($value === Entity::REQUIRED){
                    throw new \Exception(get_class($this) .
                        ' : Une ou plusieurs propriétés requises pour l\'initialisation sont manquantes');
                    return false;
                }
                $field_name = str_replace('data_', '', $property->name);
                $aDatas[$field_name] = $value;
            }
        }

        if(!$this->pdom->insert($this->db_table_label, $aDatas)){
            throw new \Exception(get_class($this) .
                ' : L\'enregistrement en Base de données à échoué');
            return false;
        }

        $this->id = $this->pdom->lastInsertId();
        $inheritingClassName = get_class($this->childRef);
        $id_property_name = 'data_' . $inheritingClassName::DATABASE_ID_LABEL;
        $prop = $this->reflect->getProperty($id_property_name);
        $prop->setAccessible(true);
        $prop->setValue($this->childRef, $this->id);

        $this->setLastId($this->id);
        return true;
    }

    //--------------------------------------------------------------------------------------------------------
    // update
    //--------------------------------------------------------------------------------------------------------
    // update tout les champs modifiés via la méthode setElement en base de données
    //--------------------------------------------------------------------------------------------------------
    public function update()
    {
        if($this->id == null){
            throw new \Exception(get_class($this) . ' : Aucun identifiant n\'a été renseigné pour l\'entité');
            return false;
        }

        $aDatas = array();
        $aConditions = array('' . $this->db_id_label => $this->id);
        foreach ($this->aUpdatedProperties as $property)
        {
            $realName = 'data_' . $property;
            $prop = $this->reflect->getProperty($realName);
            $prop->setAccessible(true);
            $aDatas[$property] = $prop->getValue($this->childRef);
        }

        $this->pdom->update($this->db_table_label, $aDatas, $aConditions);
        $this->aUpdatedProperties = array();
    }

    //--------------------------------------------------------------------------------------------------------
    // getDatas
    //--------------------------------------------------------------------------------------------------------
    // Récupère les données en base et les attribut aux variables membres correspondantes de l'instance en cours
    // Les couples [propriété - champ] sont liés par leurs noms communs
    //--------------------------------------------------------------------------------------------------------
    protected function getDatas()
    {
        $this->flag_datas_checked = true;

        // Récupération du nom des variables membres
        $reflect = $this->reflect;
        $properties = $reflect->getProperties(\ReflectionProperty::IS_PRIVATE);
        foreach($properties as $property){
            if (substr( $property->name, 0, 5 ) === 'data_'){
                $n = str_replace('data_', '', $property->name);
                $fields[] = $n;
            }
        }
        // Récupération des données en base
        $conditions = array('' . $this->db_id_label => $this->id);
        $datas = $this->pdom->select($this->db_table_label, $fields, $conditions);

        if(count($datas) < 1){
            //throw new Exception(get_class($this) . ' : Entité non présente en BDD');
            return $this->flag_database_matched = false;
        }

        // Initialisation des variables membres de l'enfant
        foreach($fields as $property){
            $realName = 'data_' . $property;
            $prop = $this->reflect->getProperty($realName);
            $prop->setAccessible(true);
            $prop->setValue($this->childRef, $datas[0][$property]);
        }

        return $this->flag_database_matched = true;
    }

    //--------------------------------------------------------------------------------------------------------
    // isIntoDatabase
    //--------------------------------------------------------------------------------------------------------
    // Est-ce que l'entité est présente en base de donnée
    //--------------------------------------------------------------------------------------------------------
    public function isIntoDatabase()
    {
        if(!$this->flag_datas_checked)
        {
            if ($this->id == null){
                throw new \Exception(get_class($this) . ' : Aucun identifiant n\'a été renseigné pour l\'entité');
                return false;
            }   else{
                $this->getDatas ();
            }
        }

        return $this->flag_database_matched;
    }

    //--------------------------------------------------------------------------------------------------------
    // setElement
    //--------------------------------------------------------------------------------------------------------
    public function setElement($_property, $_value)
    {
        if($_value === '0' || !empty($_value)){
            $realName = 'data_' . $_property;
            if ($_property == $this->db_id_label){
                throw new \Exception('class ' . get_class($this) .
                    ' : Impossible de modifier la clé primaire de l\'entité : ' . $_property);
            }

            $prop = $this->reflect->getProperty($realName);
            $prop->setAccessible(true);

            if(property_exists($this->childRef, $realName)){
                $prop->setValue($this->childRef, $_value);
                $this->aUpdatedProperties[] = $_property;
            }	else{
                throw new \Exception('class ' . get_class($this) .
                    ' : Eléments non existant ou non accessible : ' . $_property);
                return false;
            }
        }
    }

    //--------------------------------------------------------------------------------------------------------
    // getElement
    //--------------------------------------------------------------------------------------------------------
    public function getElement($_property, $_nc_term=self::NC_PROPERTY_TERM)
    {
        $realName = 'data_' . $_property;
        if(property_exists($this->childRef, $realName))
        {
            $prop = $this->reflect->getProperty($realName);
            $prop->setAccessible(true);
            $value = $prop->getValue($this->childRef);
            if(empty($value) || $value==null || $value===self::REQUIRED){
                return $_nc_term;
            }

            return $value;
        }	else{
            throw new \Exception('class ' . get_class($this).' : Eléments non existant ou non accessible : ' . $_property);
            return false;
        }
    }

    //--------------------------------------------------------------------------------------------------------
    // getResultsBy
    //--------------------------------------------------------------------------------------------------------
    // Retourne un tableau d'instances $aInstances de la classe $_entityInheritingClass correspondant
    // aux conditions $_aConditions
    //--------------------------------------------------------------------------------------------------------
    public static function getResultsBy (
        $_entityInheritingClass,
        $_aAccurateConditions=null,
        $_hydrate=Entity::INIT_HYDRATE_ON,
        $_aGroupBy=null,
        $_aOrderBy=null,
        $_bOrderType=false,
        $_iLimit=null,
        $_iOffset=null,
        $_bOverCount=false){

        $aInstances = array();
        $datas = PDOManager::getInstance()->selectAccurate(
            $_entityInheritingClass::DATABASE_TABLE_NAME,
            array($_entityInheritingClass::DATABASE_ID_LABEL),
            $_aAccurateConditions,
            $_aGroupBy,
            $_aOrderBy,
            $_bOrderType,
            $_iLimit,
            null,
            $_iOffset);

        foreach ($datas as $data){
            $aInstances[] = new $_entityInheritingClass($data[$_entityInheritingClass::DATABASE_ID_LABEL], $_hydrate);
        }

        if($_bOverCount == true){
            $countDatas = PDOManager::getInstance()->selectAccurate($_entityInheritingClass::DATABASE_TABLE_NAME, array('count(*) AS count'), $_aAccurateConditions);
            self::$iLastOverCount = $countDatas[0]['count'];
        }

        return $aInstances;
    }

}
